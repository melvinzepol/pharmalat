<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pharmalat_rel01' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '9?3YFv5VaQcKvs@>' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YR<=f9XD}aNVU5E$Itr}}@jigm1b4Hyi.!HizYrSCEkZ-94pFGkll.=J?[p`s`5z' );
define( 'SECURE_AUTH_KEY',  'k=sIn81;91C<H=pmClQwWZ,3XB]>suQbxWN.ZTBh:(r0BcL VPT%QxtXp>($<) `' );
define( 'LOGGED_IN_KEY',    'BktTO 5lkAPR*QtXnIC>}XV>2CTD=f{y*%xPw=;,Dk=92YTb^(:ok~.{mTB_|Xxd' );
define( 'NONCE_KEY',        '3^Kfur%FEUi|gVU:>NeOizC;a!4OP#2:ibO9ga0l;qS>aF/n[Lz6z<^6/<,amN?f' );
define( 'AUTH_SALT',        '{MU%x:_n0=Jw/%J.gW@W+$ScA]%oPq/Zc<M1a,E7/W%3[1/6nmEay67>5=(Kku2x' );
define( 'SECURE_AUTH_SALT', 'g?-cA;s%8/^h*M$1y2L@6o-X>&o-@(7P|[pf8[%,3qFGkKV(x~zbpVOKTiuZ Y^j' );
define( 'LOGGED_IN_SALT',   'K8p|#(k#0;1ZqVrn[wUSmnf)o9w~oWls3~4:m{MP8k3lt`(r^VLA8S5f]]jC:*1#' );
define( 'NONCE_SALT',       'kBnR0SM!:Qr7(8H@2&C$GxZ/@t[>#5Z9zquA;F%U@YU2h;9$5+|=>!K!wsMtSabY' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ph_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
